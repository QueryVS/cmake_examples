# CMake_examples

## Cmake general information

-CMake is a build generator so you can create MakeFile or another builder too easy. Look at CMake documantation and " cmake --help #from terminal"

```
Generators

The following generators are available on this platform (* marks default):
* Unix Makefiles               = Generates standard UNIX makefiles.
  Green Hills MULTI            = Generates Green Hills MULTI files
                                 (experimental, work-in-progress).
  Ninja                        = Generates build.ninja files.
  Ninja Multi-Config           = Generates build-<Config>.ninja files.
  Watcom WMake                 = Generates Watcom WMake makefiles.
  CodeBlocks - Ninja           = Generates CodeBlocks project files.
  CodeBlocks - Unix Makefiles  = Generates CodeBlocks project files.
  CodeLite - Ninja             = Generates CodeLite project files.
  CodeLite - Unix Makefiles    = Generates CodeLite project files.
  Sublime Text 2 - Ninja       = Generates Sublime Text 2 project files.
  Sublime Text 2 - Unix Makefiles
                               = Generates Sublime Text 2 project files.
  Kate - Ninja                 = Generates Kate project files.
  Kate - Unix Makefiles        = Generates Kate project files.
  Eclipse CDT4 - Ninja         = Generates Eclipse CDT 4.0 project files.
  Eclipse CDT4 - Unix Makefiles= Generates Eclipse CDT 4.0 project files.

```
the cmake --help page

## CMake project hierarchy and learning compile

```
Project
├── CMakeLists.txt
└── main.c
```

CMakeLists.txt is executable file format for cmake command. I need new file for build generation files for easy remove.

```
Project
├── CMakeLists.txt
├── build # this files
└── main.c
```
and 

> cd build ; cmake ..

okay

## First CMake Project 

look at [CMake Base Documantation](https://cmake.org/cmake/help/latest/guide/tutorial/index.html)

first code for compile C file
```
# cmake minimum version for execute if you want, don`t write. just give warning
cmake_minimum_required(VERSION 3.10)

# project name so executing CMakeList.txt file name variable PROJECT_NAME
project(firstproject)

# add for execute 
add_executable(nameismain.o main.c)
```
**and look files**
```
Project
├── CMakeLists.txt
├── build
│   ├── CMakeCache.txt
│   ├── CMakeFiles
│   │   ├── 3.18.5
│   │   │   ├── CMakeCCompiler.cmake
│   │   │   ├── CMakeCXXCompiler.cmake
│   │   │   ├── CMakeDetermineCompilerABI_C.bin
│   │   │   ├── CMakeDetermineCompilerABI_CXX.bin
│   │   │   ├── CMakeSystem.cmake
│   │   │   ├── CompilerIdC
│   │   │   │   ├── CMakeCCompilerId.c
│   │   │   │   ├── a.out
│   │   │   │   └── tmp
│   │   │   └── CompilerIdCXX
│   │   │       ├── CMakeCXXCompilerId.cpp
│   │   │       ├── a.out
│   │   │       └── tmp
│   │   ├── CMakeDirectoryInformation.cmake
│   │   ├── CMakeOutput.log
│   │   ├── CMakeTmp
│   │   ├── Makefile.cmake
│   │   ├── Makefile2
│   │   ├── TargetDirectories.txt
│   │   ├── cmake.check_cache
│   │   ├── nameismain.o.dir
│   │   │   ├── C.includecache
│   │   │   ├── DependInfo.cmake
│   │   │   ├── build.make
│   │   │   ├── cmake_clean.cmake
│   │   │   ├── depend.internal
│   │   │   ├── depend.make
│   │   │   ├── flags.make
│   │   │   ├── link.txt
│   │   │   ├── main.c.o
│   │   │   └── progress.make
│   │   └── progress.marks
│   ├── Makefile
│   ├── cmake_install.cmake
│   └── nameismain.o
└── main.c
```
## CMake some wordlist

### VARIABLE

- set(<variable> <value>... [PARENT_SCOPE])

#### list()

Reading
-   list(LENGTH <list> <out-var>)
-   list(GET <list> <element index> [<index> ...] <out-var>)
-   list(JOIN <list> <glue> <out-var>)
-   list(SUBLIST <list> <begin> <length> <out-var>)

##### Search
  list(FIND <list> <value> <out-var>)

##### Modification
-   list(APPEND <list> [<element>...])
-   list(FILTER <list> {INCLUDE | EXCLUDE} REGEX <regex>)
-   list(INSERT <list> <index> [<element>...])
-   list(POP_BACK <list> [<out-var>...])
-   list(POP_FRONT <list> [<out-var>...])
-   list(PREPEND <list> [<element>...])
-   list(REMOVE_ITEM <list> <value>...)
-   list(REMOVE_AT <list> <index>...)
-   list(REMOVE_DUPLICATES <list>)
-   list(TRANSFORM <list> <ACTION> [...])

##### Ordering
-   list(REVERSE <list>)
-   list(SORT <list> [...])


### EXECUTE

- add_executable()


### HEADERS 

- target_link_libraries() # for .a and .so file 
- target_include_directories() # for project headers 

### File 
##### **Reading**
-   file(READ <filename> <out-var> [...])
-   file(STRINGS <filename> <out-var> [...])
-   file(<HASH> <filename> <out-var>)
-   file(TIMESTAMP <filename> <out-var> [...])
-   file(GET_RUNTIME_DEPENDENCIES [...])
##### **Writing**
-   file({WRITE | APPEND} <filename> <content>...)
-   file({TOUCH | TOUCH_NOCREATE} [<file>...])
-   file(GENERATE OUTPUT <output-file> [...])
-   file(CONFIGURE OUTPUT <output-file> CONTENT <content> [...])
##### **Filesystem**
-   file({GLOB | GLOB_RECURSE} <out-var> [...] [<globbing-expr>...])
-   file(RENAME <oldname> <newname> [...])
-   file(COPY_FILE <oldname> <newname> [...])
-   file({REMOVE | REMOVE_RECURSE } [<files>...])
-   file(MAKE_DIRECTORY [<dir>...])
-   file({COPY | INSTALL} <file>... DESTINATION <dir> [...])
-   file(SIZE <filename> <out-var>)
-   file(READ_SYMLINK <linkname> <out-var>)
-   file(CREATE_LINK <original> <linkname> [...])
-   file(CHMOD <files>... <directories>... PERMISSIONS <permissions>... [...])
-   file(CHMOD_RECURSE <files>... <directories>... PERMISSIONS <permissions>... [...])
##### **Path Conversion**
-   file(REAL_PATH <path> <out-var> [BASE_DIRECTORY <dir>] [EXPAND_TILDE])
-   file(RELATIVE_PATH <out-var> <directory> <file>)
-   file({TO_CMAKE_PATH | TO_NATIVE_PATH} <path> <out-var>)
##### **Transfer**
-   file(DOWNLOAD <url> [<file>] [...])
-   file(UPLOAD <file> <url> [...])
##### **Locking**
-   file(LOCK <path> [...])
##### **Archiving**
-   file(ARCHIVE_CREATE OUTPUT <archive> PATHS <paths>... [...])
-   file(ARCHIVE_EXTRACT INPUT <archive> [...])


### OTHER FILES 

- add_subdirectory()
- include()

# 


